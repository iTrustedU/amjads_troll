module.exports = (grunt) ->
	grunt.loadNpmTasks "grunt-contrib-uglify"
	grunt.loadNpmTasks "grunt-contrib-connect"

	grunt.initConfig
		pkg: grunt.file.readJSON "package.json"
		uglify:
			halal:			
				options:
					banner: "/*! halal.js <%= grunt.template.today('yyyy-mm-dd') %> */\n"
					compress: true
				files:
					"public/js/halal/build/halal.min.js": 
						["public/js/halal/*.*"]
		connect: 
			bla:
				options:
					keepalive: true
					port: 9000
					base: "./public"



	grunt.registerTask "default", "Logging functionality", ->
		grunt.log.write "I'm a lazy programmer, so I use CoffeeScript"

	grunt.registerTask "minify", "This shall minify the Halal", ->
		grunt.log.writeln "Minifying Halal..."
		grunt.task.run 'uglify:halal'

	#grunt.registerTask 'default', ['uglify']

	grunt.registerTask "headless-test", "Headless WebKit testing via PhantomJS", ->
		grunt.log.write "Not implemented yet"

	grunt.registerTask "connect:server", "connect:server", ->
		grunt.log.write "Starting web server...."

	
