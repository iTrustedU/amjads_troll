define(
    "halal/temp/MathUtil",
 
    [],

    function() {
		var MathUtil = {};

		MathUtil.isPointInRect = function(point, rectangle) {
			if( (point.x > rectangle.x && point.x < (rectangle.x+rectangle.w))
				&& (point.y > rectangle.y && point.y < (rectangle.y + rectangle.h)) ) {
				return true;
			}
			return false;
		}

		MathUtil.isPointInCircle = function(point, circle) {
			var dx = point.x - circle.x; 
			var dy = point.y - circle.y;
			var dist = dx * dx + dy * dy;
			return dist <= Math.pow(circle.r, 2);
 		};

		return MathUtil;
    }
);