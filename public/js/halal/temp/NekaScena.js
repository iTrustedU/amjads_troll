/*jslint browser: true*/
/*global define*/
/*global jquery*/
define(
    "halal/temp/NekaScena",

    [
    "halal/scenes/Scene",
    "halal/core/Halal",
    "halal/sprites/SSManager",
    "halal/core/DOMEventManager",
    "halal/geometry/Rectangle",
    "halal/geometry/Point",
    "halal/core/Entity",
    "halal/core/Timer"
    ],

    function(Scene, Halal, SSManager, EventManager, Rectangle, Point, Entity, Timer) {
        "use strict";

        var sprs = SSManager.getSpritesFromSheet("horses");

        var temp_circle = {
            x: 250, 
            y: 200,
            r: 50
        };
        var i = 0;
        var entities = new Array(50);

        var ent2 = new Entity();
        ent2.setSprite(sprs["horse.png"]);
        ent2.setPos(150,150);
        entities[0] = ent2;

        for(var i = 2; i < 50; ++i) {
            entities[i] = new Entity();
            entities[i].setSprite(sprs["horse.png"]);
            entities[i].setPos(Math.random() * 640, Math.random() * 480);
        };

        var ent1 = new Entity();
        ent1.setPos(300,200);
        ent1.setSprite(sprs["deer.png"]);
        entities[1] = ent1;

        Halal.renderer.goFullScreen();

        var NekaScena = Scene.extend({
            render: function(delta) {
                for(i = 0; i < 50; ++i) {
                    entities[i].draw(delta);
                    entities[i].drawBBox();
                }
                Halal.renderer.strokeCircle(temp_circle, "red");
            },
            update: function(delta) {
                ent1.update(delta);
            },
        });

    function drawBBox(sprite) {
        Halal.strokeRect("red", sprite.getBBox());
    }

    var bbox = new Rectangle(0,0,50,50);
    var scena = new NekaScena("neka_scena", bbox);

    //on a single entity or on a group of entities
    //group of scenes, or whatever :)
/*    Halal.when("MOUSE_LEFT_CLICK").on(ent1).do(function(coords) {

    });*/

    EventManager.register(scena, EventManager.events.KEYDOWN, function(ev) {
        console.log(ev);
        if(ev.keyCode == 32) {
            console.log('to je to');
            Halal.renderer.goFullScreen();
        }
    });

    EventManager.register(ent1, EventManager.events.CLICK, function() {
        var angle = angle = Math.PI/190;
        //alert('bla');
        ent1.timer.each(16, function() {
            Halal.renderer.ctx.translate(ent1.posVec.x+ent1.sprite.w/2, ent1.posVec.y+ent1.sprite.h/2);       
            Halal.renderer.ctx.rotate(angle); 
            Halal.renderer.ctx.translate(-ent1.posVec.x-ent1.sprite.w/2, -ent1.posVec.y-ent1.sprite.h/2);  
        });
    });

    /*scena.on(EventManager.events.CLICK, function() {
        console.log("click na scenu");
    });*/

    //kad je u fokusu, onda primaj ovo
/*    Halal.when("KEYDOWN").on([scena,scena1]).do(function() {

    });

    Halal.when("KEYDOWN.ESCAPE").do(function(key)) {

    });*/

    Halal.addScene(scena);
});