define(
	"halal/scenes/Scene", 

	[
	 "halal/core/Class"
	],

	function(Class) {
		var Scene = Class.extend({
			init: function(id, bbox) {
				this.id = id;
				this.bbox = bbox;
			},
			setID: function(id) {
				this.id = id;
			},
			getID: function() {
				return this.id;
			},
			update: function(delta) {
				
			},
			render: function() {},
			getArea: function() {
				return this.bbox;
			}
		});
		return Scene;
	}
);