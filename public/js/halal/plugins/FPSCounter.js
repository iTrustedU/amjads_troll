define(
    "halal/plugins/FPSCounter",

    [
        "jquery",
        "halal/core/StateManager",
        "halal/core/DOMManager"
    ],

    function($, StateManager, DOMManager) {
    	var div = $(DOMManager.createDivAt(DOMManager.getWidth(), 0));
    	$(div).css({
            "font-family": "Courier",
            "font-size": "0.8em",
            "background-color": "rgba(124,98,122,0.5)",
            "color": "white"
        }).text("FPS: 00");

     	DOMManager.add("fpscounter", div.get(0));
    	StateManager.when("FPS_UPDATED", function(num) {
            div.text("FPS: " + num);
    	});
    }
);