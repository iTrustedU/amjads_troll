define(
    "halal/plugs/LoaderProgressBar",
 
    ["jquery-ui",
     "halal/core/DOMManager"],

    function($, DOMManager) {

        var val = 0.0001;
        var factor = 0;

        var progressWrap = $("<div/>", {
            "css": {
                "height": "200px",
                "float": "left",
                "left": "50%",
                "margin-left": "-85px",
                "z-index": "1000",
                "position": "relative",
                "margin-top": -DOMManager.getHeight()/2
            }
        });

    	var progress = $("<div/>", {
    		"css": {
    			"width": "170px",
                "height": "20px",
    		}
    	});

        progress.appendTo(progressWrap);
    	progressWrap.appendTo(DOMManager.getRenderSpace);
    	progress.progressbar({
    		value: val
    	});

        var progressText = $("<span/>", {
            "css": {
                "position": "relative",
                "text-align": "center",
                "margin": "0 auto",
                "width": "170px",
                "font-family": "Quintessential",
                "font-size": "0.6em",
                "display": "block"
            }
        });

        progressText.text("Loading...");
        progressText.appendTo(progressWrap);

		Halal.when("RESOURCE_LOADED", function(name, prev) {
			val += factor;
            animateProgress(val, name);
    	});

    	Halal.when("RESOURCES_LOADING_FINISHED", function(num) {
            animateProgress(100);
    	});

        Halal.when("RESOURCES_LOADING_STARTED", function(num) {
            factor = 100/num;
        });

    	Halal.when("ENGINE_STARTED", function() {
            $(".ui-progressbar-value").promise().done(function() {
                progressWrap.remove(); 
                require(["halal/temp/NekaScena"], function() {
                    console.log("neka scena loaded");
                });
            });
    	});

        function animateProgress(val, name) {
            $(".ui-progressbar-value").animate(
            {
              width: (val)+"%",
              queue: true,
              duration: "fast"
            }, function() {
                progress.progressbar("value", val);
                if(name) {
                    progressText.html("Loaded resource: " + "<b>"+name+"</b>")
                }
            });
        }
    }
);