define(
    "halal/plugins/FullScreenButton",
 
    ["halal/renderers/CanvasRenderer",
    "halal/core/DOMManager"],

    function(Renderer, DOMManager) {
		var div = $(DOMManager.createDivAt(DOMManager.getWidth() - 5, DOMManager.getHeight() - 15, 20));
		DOMManager.add("fullscreenbtn", div.get(0));
		div.css({
			"background-color": "red",
			"text-align": "center",
			"font-size": "0.6em"
		});
		div.text("X");
		div.on("click", function() {
			Renderer.goFullScreen();
		})
    }
);