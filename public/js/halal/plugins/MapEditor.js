define(
    "halal/plugins/MapEditor",
 
    ["halal/core/StateManager", "halal/core/Storage"],

    /* Lets you search and fallback for on supported technologies */
    function(StateManager, Storage) {
        function create_li(img, name) {
            var li = $('<li/>',
            {
                "id": name,
                "class": "ui-state-default",
                "css": {
                    "overflow": "hidden",
                    "display": "none"
                }
            });

            li.hover(function() {
                $(this).css({"border": "1px solid #ff7f50"});
            }, function() {
                $(this).css({"border": "1px solid #ccc"});
            });

            var img = $("<img/>", {
                "src": img
            });

            img.css({
                "display": "block",
                "margin": "auto",
                "width": "75%",
                "height": "75%"
            });

            var title = $("<div/>", {
                "width": "inherit",
                "height": "25%",
                "css": {
                    "display": "block",
                    "text-align": "center",
                    "font-size": "0.8em"
                }
            });

            title.text(name);
            li.append(img);
            li.append(title);
            return li;
        }

        StateManager.when("SPRITESHEET_LOADED", function(url, img, name) {
            
            $("#sprites-container").slideUp();
            $("#animation-container").slideUp();

            var li = create_li(img, name);

            li.click(function() {
                var id = $(this).attr("id");
                var sprs = Storage.spritesheets[id].sprites;
                var sprsheet_w = Storage.spritesheets[id].meta.size.w;
                var sprsheet_h = Storage.spritesheets[id].meta.size.h;

                $("#sprites-container").empty();
                $("#sprites-container").slideDown();
                for(var spr in sprs) {
                    var li = create_li(sprs[spr].img, spr);
                    $("#sprites-container").append(li);
                    li.fadeIn();
                }
            });

            $("#spritesheets-container").append(li);
            $("#spritesheets-container, #sprites-container").parent().find("legend").click(function(evt) {
                evt.stopPropagation();
                evt.preventDefault();
                evt.stopImmediatePropagation()
                $(this).parent().find("ol").slideToggle("fast", "swing");
            });

            li.fadeIn();
        });

		/** --enable-memory-info
		 * Part of a V8 engine switch to Chrome
		 * Ovde treba da detektujem useragent
		 * mozda bolje yepnope da koristim?
		 * ili modernizr? da li modernizr radi proveru nad performance apijem?
		 * 	- ne radi, which calls for pull request
		 */
    }
);