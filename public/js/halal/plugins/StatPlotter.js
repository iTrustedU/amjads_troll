define(
    "halal/plugins/StatPlotter",
 
    [
    	"jquery",
    	"loglevel",
    	"modernizr",
    	"halal/core/DOMManager",
    	"halal/core/Timer",
    	"halal/core/StateManager",
    	"flot"
    ],

    function($, Log, Modernizr, DOMManager, Timer, StateManager) {
    	if(!Modernizr.ischrome) {
    		Log.info("StatPlotter is supported on chrome only");
    	}
		var div = $(DOMManager.createDivAt(DOMManager.getWidth() - 5, 5, 300,170));
		DOMManager.add("plotstat", div.get(0));
		
		var Statistics = {
			fpsData: [],
			heapData: []
		};

		Timer.each(1000, function() {
			$.plot(div, [Statistics.heapData, Statistics.fpsData], {
				lines: { show: true, fill: true},
				grid: {
					borderColor: "#ffffff",
					minBorderMargin: null
				}
			});
			$('.tickLabel').css({
				"font-size": "0.6em"
			});
		});

		StateManager.when("FPS_UPDATED", function(fps) {
			var t = Timer.total/1000;
			var heapMB = window.performance.memory.usedJSHeapSize / (1024*1024);
			Statistics.heapData.push([t, heapMB]);
			Statistics.fpsData.push([t, fps]);
		});			
    }
);