define(
    "halal/plugins/BrowserTests",
 
    ["modernizr"],

    function(Modernizr) {
	    Modernizr.addTest("ischrome", function() {
    		return !!navigator.userAgent.match(/chrome/i);
    	});
    }
);