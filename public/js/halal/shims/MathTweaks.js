define(
    "halal/shims/MathTweaks",
 
    [],

    function() {
		//to float typed array or not?
		if(!MATH_ARRAY_TYPE) {
			var MATH_ARRAY_TYPE = (typeof Float32Array !== 'undefined') ? Float32Array : Array;
		}

		if(!MATH_EPSILON) {
			var MATH_EPSILON = 0.00001; //veca preciznost od ove mi nikad nece trebati
		}

		function degToRad(deg) {
			return (deg / 180 * Math.PI) >> 0;
		}

		function radToDeg(rad) {
			return (rad / Math.PI * 180) >> 0;
		}
    }
);