define(
    "halal/shims/RAFShim",
 
    [],

    function() {
		window.requestAnimFrame = (function() {
			return window.requestAnimationFrame || 
				   window.webkitRequestAnimationFrame || 
				   window.mozRequestAnimationFrame || 
				   function(callback) {
				   	window.setTimeout(callback, 16); //1000 / 60
				   };
		})();
    }
);