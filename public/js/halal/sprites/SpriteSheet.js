define('halal/sprites/SpriteSheet', function() {
 
    var SpriteSheet = function(path, img, meta, sprites) {
        this.path = path;
        this.img = img;
        this.meta = meta;
        
        this.enc = undefined;
        this.sprites = sprites || {};
    };

    SpriteSheet.prototype = {
    	putSprite: function(key, spr) {
    		this.sprites[key] = spr;
    	},
        storeBase64: function(enc) {
            this.enc = enc;
        }
    }

    return SpriteSheet;
});