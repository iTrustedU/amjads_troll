define("halal/sprites/SpriteFactory", 
    ['halal/sprites/Sprite'], 

function(Sprite) {
    var SpriteFactory = {};

    SpriteFactory.fromSpriteSheetFrame = function fromName(frame, img) {
        var sprite = new Sprite(
            frame.x, frame.y,
            frame.w, frame.h, 
            img
        );
        return sprite;
    }

    return SpriteFactory;
});