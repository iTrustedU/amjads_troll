define(
    "halal/plugins/BrowserTests",
 
    ["modernizr"],

    /* Lets you search and fallback for on supported technologies */
    function(Modernizr) {
    	Modernizr.addTest("ischrome", function() {
    		return !!navigator.userAgent.match(/chrome/i);
    	});
		/** --enable-memory-info
		 * Part of a V8 engine switch to Chrome
		 * Ovde treba da detektujem useragent
		 * mozda bolje yepnope da koristim?
		 * ili modernizr? da li modernizr radi proveru nad performance apijem?
		 * 	- ne radi, which calls for pull request
		 */
    }
);