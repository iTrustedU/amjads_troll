define(
    "halal/core/UUID",
 
    [],

    function() {
    	var uuid_ = 0xDEADBEEF;
		var UUID = {
			create: function() {
				uuid_++;
				return uuid_;
			}
		}
		
		return UUID;
    }
);