define(
    "halal/core/Entity",
 
    ["halal/geometry/Vec2", 
     "halal/sprites/Sprite",
     "halal/renderers/CanvasRenderer",
     "halal/geometry/Rectangle"],

    function(Vec2, Sprite, Renderer, Rectangle) {
        /*var time = 5000; //2 sekunde
        var so_far = 0;
        var move_by = undefined;
        var angle = undefined;
        var prc = undefined;
        */

    	var Entity = function Entity(x, y) {
    		this.sprite = undefined;
			this.posVec = Vec2.fromValues(x, y);
            this.velocity = 100;
            this.old_pos = this.posVec;
    	};

    	Entity.prototype = {
    		getID: function() {
    			return 0;
    		},
    		draw: function() {
    			Renderer.drawSprite(
    				this.sprite, 
    				this.posVec[0], 
    				this.posVec[1]
    			);
    		},
    		setPos: function(x, y) {
    			this.posVec.x = x;
    			if(y)
    				this.posVec.y = y;

    			if(this.sprite) {
    				this.bbox = this.getBBox();
    			}
    		}, 
    		setSprite: function(spr) {
    			this.sprite = spr;
    			this.bbox = this.getBBox();
    		},
    		getBBox: function(sc) {
                sc = sc || 1;
				var rect = new Rectangle(
					this.posVec.x,
					this.posVec.y * sc,
					this.sprite.w * sc,
					this.sprite.h * sc
				); 
                return rect;	
    		},
    		drawBBox: function() {
    			Renderer.strokeRect("red", this.bbox);
    		},
            update: function(delta) {
            }
    	};


        function cosinterp(from, to, delta) {
            angle = delta * Math.PI;
            prc = (1 - Math.cos(angle)) / 2;
            return from + (to-from) * prc;
        }

    	return Entity;
    }
);