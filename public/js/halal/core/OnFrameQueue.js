define(
    "halal/core/OnFrameQueue",
 
    ["halal/core/StateManager"],

    /**
     * Enables batch processing of events in RAF queue
     */
    function(StateManager) {
    	//This is suspectible to weird tweakings
    	var MAX_NUM_OF_EATERS = 50,
    	    queue_len_ = 0, i = 0,
			queue_ = new Array(MAX_NUM_OF_EATERS);

		var OnFrameQueue = function OnFrameQueue() {};

		StateManager.register(["ENTER_FRAME"]);

    	StateManager.when("ENTER_FRAME", function() {
    		for(i = 0; i < queue_len_; ++i) {
    			+(queue_.pop())();
    		};
    	});

		OnFrameQueue.prototype = {
			/**
			 * Let's push some frame eaters!!!
			 */
			push: function(frameEater) {
				queue_.push(frameEater);
				queue_len_++;
			}
		};

        return OnFrameQueue;
    }
);