define(
    "halal/core/StateManager",
 
    [
    	"loglevel"
    ],
    
    /**
     * attaching properties to engine itself won't hog the engine in any way
     * as it is in pre-init stage
     */
    function(Log) {
    	var MAX_NUM_OF_STATES = 50,
    		current_state = undefined,
    		detach_pending = [],
    		states_map = [];

    	var i = 0;
        /**
         * Helper object 
         */
    	var DetachExecutor = function DetachExecutor(initState, ind) {
    		this.init_state = initState;
    		this.index = ind;
    	};

    	DetachExecutor.prototype = {
    		/**
    		 * Detach after a certain number of handlings 
    		 * or after a state transition
    		 * @param {Number or String} arg
    		 */
    		after: function(arg) {
    			detach_pending.push({
    				state: this.init_state,
    				index: this.index,
    				after: arg || 1
    			});
    		}
    	}

    	var StateManager = {
    		when: function(state, callback) {
    			var len = states_map[state].push(callback);
    			return new DetachExecutor(state, len - 1);
    		}, 

    		register: function(statesArray) {
    			for(i in statesArray) {
    				if(!states_map[statesArray[i]]) {
    					states_map[statesArray[i]] = [];
    				}
    			}
    		},

    		/*
    		 * @param {String} state Next transition state
    		 * @param {Object} args An object containing misc data
    		 */
    		trigger: function(state) {
    			var trig = undefined;
    			for(i in states_map[state]) {
                    Log.info("[" + current_state + "->" + state + ", " + Array.prototype.slice.call(arguments,1) + "]");
    				trig = states_map[state][i];
                    var args = Array.prototype.slice.call(arguments,1);
                    if(trig) {
    				    trig.apply(null, args);
                    }
    			}

    			var obj = undefined;
    			for(i in detach_pending) {
    				obj = detach_pending[i];
    				if( (typeof obj.after === 'number') && 
                        (obj.state === state) ) {
	    				obj.after--;
	    				if(obj.after === 0) {
                            //once in a while (say every 10 seconds)
                            //clear this shit up
	    					states_map[obj.state][obj.index] = undefined;
	    				}
    				}
    				else if(obj.state === state) {
                        //here to, as to why - see 
                        /* @see http://coding.smashingmagazine.com/2012/11/05/writing-fast-memory-efficient-javascript/ */
	    				states_map[state][obj.index] = undefined;
    				}
    			}
                current_state = state;
    		},

            currentState: function() {
                return current_state;
            }
    	};

    	return StateManager;
    }
);