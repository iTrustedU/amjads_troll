/* 
 * This shit right here is ma man Resig work, I just 
 * addopted it a bit
 */
define('halal/core/Class', function() {
	var initializing = false;
	var Class = function() {};

	/**
	 * let's check if browser supports decompilation
	 * how it works? well, test function parameter coerces to string 
	 * and if it does, test should pass as xyz is in passed function body 
	 */
	var fnTest = /xyz/.test(function() {xyz;}) ? /\b_super\b/ : /.*/;

	/* prop should be an object literal */
	Class.extend = function(prop) {
		//let's copy all previous props
		var _super = this.prototype;
		
		//let's create a new class that shall be added 
		//all of prop props
		initializing = true;
		var prototype = new this();
		initializing = false;

		for(var name in prop) {
			if(typeof prop[name] === "function" &&
				typeof _super[name] === "function" &&
				fnTest.test(prop[name])) {
				//need to capture variables in this closure
				prototype[name] = (function(name, fn) {
					return function() {
						var tmp = this._super;
						this._super = _super[name];
						var ret = fn.apply(this, arguments);
						this._super = tmp;
						return ret;
					};
				}(name, prop[name]));
			} else {
				prototype[name] = prop[name];
			}
		}

		/* Dummy constructor */
		function Class() {
			if( !initializing && this.init ) {
				this.init.apply(this, arguments);
			}
		}

		Class.prototype = prototype;
		Class.prototype.constructor = Class;
		Class.extend = arguments.callee;
		return Class;
	};
	
	return Class;
});