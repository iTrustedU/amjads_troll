define(
    "halal/core/Storage",
 
    ["modernizr"],

    function(Modernizr) {

    	var Storage = function() {
			this.plugins = {};
    		this.spritesheets = {};
    		this.sprites = {};
    		this.sounds = {};
    		this.animations = {};

			if(!Modernizr.localstorage) {
				return;
			}

			for(var i in this) {
				if(typeof this[i] === 'function') {
					continue;
				}
				if(!window.localStorage[i]) {
					window.localStorage[i] = "{}";
				} else {
					/* some form of versioning diff should 
					 * be in place here */
					this[i] = this.deserialize(i);
				}
			}
		};

		if(Modernizr.localstorage) {
			Storage.prototype = {
				serialize: function(where, val, key) {
					var oldArr = this.deserialize(where);
					if(!key) {
						oldArr = val;
					} else {
						oldArr[key] = val;
					}
					this[where] = oldArr;
					window.localStorage.setItem(where, JSON.stringify(oldArr));
				},

				deserialize: function(where, key) {
					if(key) {
						return JSON.parse(window.localStorage.getItem(where))[key];
					}
					return JSON.parse(window.localStorage.getItem(where));
				}
			}
		} else {
			Storage.prototype = {
				serialize: function(where, key, value) {
					if(value) {
						this[where][key] = value;
					} else {
						this[where].push(key);
					}
				},

				deserialize: function(where, key) {
					if(key) {
						return this[where][key];
					} 
					return this[where];
				}
			}
		}

		return new Storage();
    }
);