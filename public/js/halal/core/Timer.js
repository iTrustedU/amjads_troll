define(
    "halal/core/Timer",
 
    ["halal/core/UUID",
     "halal/core/Halal",
     "halal/shims/HResTimerShim"],

     /** @todo treba dobro da se proceslja ovaj tajmer **/
    function(UUID, Halal) {
		var i = 0;
		var new_time = undefined;
		
		var Timer = function() {
			this.trig_len = 0;
			this.time = window.performance.now();
			this.delta = 0.0;
			this.total = 0.0;
			this.accum = 0.0; //miliseconds
			this.trigger_time = Number.INFINITY; //default triggering time
			this.triggers = [];
		}

		Timer.prototype = {
			tick: function() {
				new_time = window.performance.now();
				this.delta = new_time - this.time;
				this.time = new_time;
				this.total += this.delta;
				for(i = 0; i < this.trig_len; ++i) {
					this.triggers[i].accum += this.delta;
					if(this.triggers[i].accum >= this.triggers[i].trig_time) {
						this.triggers[i].trig_count++;
						this.triggers[i].callback(this.triggers[i].trig_count);
						this.triggers[i].accum = 0;
					}
				}
			},

			each: function(trigTime, callb) {
				this.triggers.push({
					trig_count: 0,
					accum: 0,
					callback: callb,
					trig_time: trigTime
				});
				return (++this.trig_len);
			},
			/* this shouldn't work at the moment */
			stop: function(id) {
				this.trig_len--;
				delete this.triggers[id];
			},

			attachTrigger: function(callb) {
				this.triggers.push(callb);
				this.trig_len++;
			}

			/*setTrigTime: function(time) {
				this.trigger_time = time;
			}*/
		}

		return new Timer();
    }
);