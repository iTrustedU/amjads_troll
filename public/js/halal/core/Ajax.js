define(
    "halal/core/Ajax",
 
    [],


    /** 
     * This mimicks jQuery's get functionality
     */
    function(DeferredObject) {
		var Ajax = {}; 

		var Result = function Result() {};
		Result.prototype = {
			url: "N/A",
			succ_: undefined,
			fail_: undefined,
			always_: undefined,
			fail: function(callb) {this.fail_ = callb;},
			always: function(callb) {this.always_ = callb;},
			success: function(callb) {this.succ_ = callb;}
		}

		Ajax.get = function(url, callback) {
			var result = new Result();
			var xhttpreq = new XMLHttpRequest();
			xhttpreq.open("GET", url);
			xhttpreq.send();

			result.succ_ = callback;
			result.url = document.domain + '/' + url;

			xhttpreq.onreadystatechange = function() {
				if((xhttpreq.readyState == 4)) {
					var type = xhttpreq.getResponseHeader("Content-Type");
					if(xhttpreq.status == 200) {
						var data = xhttpreq.responseText;
						if(type === "application/json") {
							data = JSON.parse(data);
						}
						result.succ_(data);
					} else if(result.fail_) {
						result.fail_();
					}
					
					if(result.always_) {
						result.always_();
					}
				}
			};
			return result;
		};

		/** @todo **/
		Ajax.post = function() {
		};

		return Ajax;
    }
);