define(
    "halal/core/AssetManager",
 
    [
    	"halal/core/DeferredObject",
     	"halal/core/StateManager",
     	"halal/core/Ajax",
     	"halal/sprites/SpriteSheet",
		"halal/sprites/SpriteFactory",
		"halal/core/Storage"
    ],

    /** 
     *  what needs to be done:
	 *  - indexedDB -> localStorage -> none
	 *    - fallback chain for data persistence
	 *  - utilizing webworkers for async loading
	 *  - mipmapping spritesheets on the fly
	 *  - calculating bounding boxes on the fly?
     */

	/* 
	 * loadAnimations();
	 * loadSpritesheets();
	 * loadSounds();
	 * loadParticles([]);
	 * loadPlugins([]);
	 * loadScenes(); 
	 * loadEntities();
	 * loadBehaviors();
	 * loadShims();
	 */

	 /* 
	  * getAnimation();
	  * getSprite();
	  * getSpritesheet();
	  */

	 /* Tugo moja, localStorage na hromu je samo 2.5mb jebena!!! */
	 // ne vredi onda, moram imati fallback na neku drugu varijantu

    function(DeferredObject, StateManager, Ajax, SpriteSheet, SpriteFactory, Storage) {
    	var plugins_path = "halal/plugins/";
    	var spritesheets_path = "resources/spritesheets/";
    	var resources_path = "resources/resources.json";
    	var AssetManager = {};

    	StateManager.register([
    		"PLUGIN_LOADED",
    		"PLUGIN_LOADING_FAIL",
    		"PLUGINS_LOADED",
    		"CONFIG_LOADED",
    		"SPRITESHEETS_LOADED",
    		"SPRITESHEETS_LOADING",
    		"SPRITESHEET_LOADING_FAIL",
    		"SPRITESHEET_LOADING",
    		"SPRITESHEET_LOADED"
    	]);

    	AssetManager.sprToBase64 = function(sheet64, frame) {
    		var canvas = document.createElement("canvas");
    		canvas.width = frame.w;
    		canvas.height = frame.h;
    		var ctx = canvas.getContext("2d");
    		var img = document.createElement("img");
    		img.src = sheet64;
    		ctx.drawImage(
				img, 
				frame.x, 
				frame.y,
				frame.w,
				frame.h,
				0,
				0,
				frame.w,
				frame.h
			);
			var url = canvas.toDataURL();
			delete canvas;
    		delete ctx;
    		delete img;
			return url;
    	};

    	AssetManager.sprsheetToBase64 = function(img) {
    		var canvas = document.createElement("canvas");
    		canvas.width = w;
    		canvas.height = h;
    		var ctx = canvas.getContext("2d");
	    	ctx.drawImage(img, 0, 0);
	    	var url = canvas.toDataURL("image/png");
	    	delete canvas;
	    	delete ctx;
	    	delete img;
    		return url;
    	};

    	AssetManager.serializePlugin = function(nm, plug) {
    		var oldPlugins = Storage.deserialize("plugins");
    		oldPlugins[nm] = plug || null;
    		Storage.serialize("plugins", oldPlugins);
    	}

    	AssetManager.serializeSpritesheet = function(name, sheet) {
    		Storage.serialize("spritesheets", sheet, name);
    	}

    	AssetManager.serializeSprite = function(sheetname, name, sprite) {
    		var sheet = Storage.deserialize("spritesheets")[sheetname];
    		sheet.sprites[name] = sprite;
    		Storage.serialize("spritesheets", sheet, sheetname);
    	}

    	AssetManager.loadPlugins = function(plugs) {
			var defer = new DeferredObject(plugs.length);
			for(var i = 0; i < plugs.length; ++i) {
				(function(nm) {
					require([plugins_path + nm], function(plug) {
						StateManager.trigger("PLUGIN_LOADED", plugins_path + nm);
						AssetManager.serializePlugin(nm, plug);
						defer.acquire();
					});
				}(plugs[i].name));
			}
			return defer.promise().done(function() {
				StateManager.trigger("PLUGINS_LOADED");
			});
    	};

    	AssetManager.loadConfig = function(file) {
    		var glob_defer = new DeferredObject(1);
    		var local_defer = new DeferredObject(1000000);
    		var request = Ajax.get(file);
    		var config = undefined;

    		var res_acquired = function() {
    			local_defer.acquire();
    		}

    		request.success(function(content) {
    			config = content;
				local_defer.num_events = Object.keys(config).length;

				StateManager.when("PLUGINS_LOADED", function() {
					if("spritesheets" in config) {
	    				AssetManager.loadSpritesheets(config.spritesheets).done(res_acquired);
					}
				});

				if("plugins" in config) {
					AssetManager.loadPlugins(config.plugins).done(function() {
						res_acquired();
					});
				}
    		});

    		local_defer.promise().done(function() {
    			glob_defer.acquire(config);
    		});

    		return glob_defer.promise();
    	};

	    /**
	     * Loads spritesheets asynchronously
	     */
    	AssetManager.loadSpritesheets = function(sheets) {
    		var numRetreieved = 0;

    		function parseTexPackJSON(sheet) {
    			var sheets = sheet.files;
    			var total = sheets.length;
		        var resDefer = new DeferredObject(total);

		        var onGetFail = function() {
		            StateManager.trigger("SPRITESHEET_LOADING_FAIL", this.url)
		        };

		        var onGetAlways = function() {
		            resDefer.acquire();
		        };

		        var onGetSuccess = function(sheetJSON) {
		            var extractSheetName = /.*\/(.*)\.json/;
		            numRetreieved++;

		            var name = this.url.match(extractSheetName)[1];
		            

		            var imgSrc = spritesheets_path + sheetJSON.meta.image;
		            var imgz = document.createElement("img");
		            imgz.w = sheetJSON.meta.size.w;
		            imgz.h = sheetJSON.meta.size.h;
		            console.log(imgSrc);
		            var url = this.url;

		            imgz.onload = function() {
			            if(!Storage.spritesheets[name]) {
			                var sh = new SpriteSheet(
			                    url,
			                    AssetManager.sprsheetToBase64(imgz),
			                    sheetJSON.meta
			                );
			                AssetManager.serializeSpritesheet(name, sh);
			            }
			            
			            for(var sname in sheetJSON.frames) {
			                var sprite = SpriteFactory.fromSpriteSheetFrame(
			                    sheetJSON.frames[sname].frame, 
			                    AssetManager.sprToBase64(Storage.spritesheets[name].img, sheetJSON.frames[sname].frame)
			                );
			                AssetManager.serializeSprite(name, sname, sprite);
			            }

			            StateManager.trigger("SPRITESHEET_LOADED", this.url, Storage.spritesheets[name].img, name);
		            };
		        };

	    		StateManager.trigger("SPRITESHEETS_LOADING", total, spritesheets_path);

		        for(var i = 0; i < total; ++i) {
		            var url = spritesheets_path + sheets[i].path;
		            var request = Ajax.get(url); 
		            StateManager.trigger("SPRITESHEET_LOADING", url);
		            request.success(onGetSuccess);
		            request.fail(onGetFail);
		            request.always(onGetAlways);
		        };

	        	return resDefer.promise();
	    	};

	        var defer = new DeferredObject(sheets.length);
	        for(var i = 0; i < sheets.length; ++i) {
	        	spritesheets_path = sheets[i].path;
	        	parseTexPackJSON(sheets[i]).done(function() {
	            	defer.acquire(numRetreieved);
	        	});
	        }

	        return defer.promise().done(function(num) {
	            StateManager.trigger("SPRITESHEETS_LOADED", num);
	        });
   		}

	    /**
	     * Retreives sprite from sheet
	     */
	    AssetManager.getSprite = function(sheet, sprname) {
	        var sprite = Storage.spritesheets[sheet].sprites[sprname];
	        
	        if(!sprite) {
	            return null;
	        }

	        if(typeof sprite.img === "string") {
	        	var img = new Image();
	        	img.src = sprite.img;
	        	sprite.img = img;
	        }
	        
	        return sprite;
	    }

	    AssetManager.getSpritesheets = function() {
	    	
	    };

	    AssetManager.getSpritesFromSheet = function(sheet) {

	    }

		return AssetManager;
    }
);