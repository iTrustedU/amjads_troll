define(
    "halal/core/DOMManager",
 
    ['halal/core/StateManager'],

    function(StateManager) {
		var DOMManager = {};

		var space = document.getElementById("domlayer"),
			spaceRect = space.getBoundingClientRect(),
			elements = [];

		DOMManager.getRect = function() {
			return spaceRect;
		};

		DOMManager.getDomLayer = function() {
			return space;
		};

		DOMManager.getHeight = function() {
			return spaceRect.height;
		};

		DOMManager.getWidth = function() {
			return spaceRect.width;
		};

		DOMManager.createDivAt = function(x, y, w, h) {
			var div = document.createElement("div");
			if(!w) w = 0;
			if(!h) h = 0;
			div.style.position = "absolute";
			div.style.display = "block";
			if(w > 0)
				div.style.width = w + "px";
			if(h > 0)
				div.style.height = h + "px";
			div.style.left = (x - w) + "px";
			div.style.top = (y) + "px";

			return div;
		};

		DOMManager.add = function(id, div) {
			//div.style["z-index"] = "-1000";
			div.id = id;
			space.appendChild(div);
			elements.push(id);
		};


		DOMManager.clear = function() {
			for(var i = 0; i < elements.length; ++i) {
				console.log(elements[i]);
			};
		}
		return DOMManager;
    }
);