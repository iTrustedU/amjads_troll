define(
    "halal/core/ScreenManager",
 
    ["halal/core/StateManager",
     "halal/core/Timer",
     "halal/core/UUID"],

    function(StateManager, Timer, UUID) {
    	var screens = [];
    	var i = 0,
    		num_screens = 0;

		var ScreenManager = {
			manage: function(screen) {
				console.log(typeof screen);
				num_screens = screens.push(screen);
				return num_screens;
			},

			update: function(delta) {
				/*for(i = 0; i < num_screens; ++i) {
					screens[i].update();
				}*/
			},

			render: function(delta) {
				for(i = 0; i < num_screens; ++i) {
					screens[i].render();
				}
			}
		};

		return ScreenManager;
    }
);