define(
	"halal/core/DOMEventManager", 
	
	["loglevel",
	 "halal/geometry/Point",
	 "halal/temp/MathUtil"], 

	function(Log, Point, MathUtil) {
		var eventBuffer = [];

		var DOMEventManager = {
			events : {
				CLICK : 0,
				KEYDOWN: 1,
			}
		};

		/*
		//Hendlovanje klikova
		//Treba implementirati nesto slicno kao u jQ 
		//Event bubbling or not
		Halal.DOMViewPort.addEventListener('click', function(ev) {
			var len = eventBuffer[DOMEventManager.events.CLICK].length;
			for(var i = 0; i < len; ++i) {
				var hit = eventBuffer[DOMEventManager.events.CLICK][i];
				//ovo treba izmisliti, da li proveravam nad poligonom 
				//ili ipak samo nad rectom
				console.log(getMousePos(ev));
				if(MathUtil.isPointInRect(getMousePos(ev), hit.entity.getBBox(Halal.clientRect.scale))) {
					Halal.pushEvent("click happened", hit.callback);
				}
			}
		});

		Halal.DOMViewPort.addEventListener('keydown', function(ev) {
			var len = eventBuffer[DOMEventManager.events.KEYDOWN].length;
			for(var i = 0; i < len; ++i) {
				var hit = eventBuffer[DOMEventManager.events.KEYDOWN][i];
				hit.callback(ev, Halal.timer.delta);
			}
		});

		DOMEventManager.register = function(obj, event, callback) {
			if(!eventBuffer[event]) {
				eventBuffer[event] = [];
			}
			eventBuffer[event].push(
				{
					entity: obj,
					callback: callback
				}
			);
		};
		*/
		
		/* @todo Treba mi hendlovanje dragginga i dropinga*/
		/* Jako lose resenje, ali za sada je kul */

	    function getMousePos(evt) {
	        return new Point(
	        	(evt.clientX - Halal.clientRect.left),
	        	(evt.clientY - Halal.clientRect.top)
	        );
	    }

		return DOMEventManager;
	}
);