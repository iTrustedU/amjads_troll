define(
    "halal/core/CircularBuffer",
 
    [],

    function() {
		var CircularBuffer = function(size) {
			this.size = size + 1;
			this.buffer = new Array(this.size + 1);
			for(var i = 0; i < this.size; ++i) {
				this.buffer[i] = null;
			}
			this.start = 0;
			this.end = 0;
		};

		CircularBuffer.prototype = {
			push: function(element) {
				this.buffer[this.end] = element;
				this.end = (this.end + 1) % (this.size);
				if(this.end == this.start) {
					this.start = (this.start + 1) % (this.size);
				}
			},

			pop: function() {
				var ret = this.buffer[this.start];
				this.start = (this.start + 1) % this.size;
				return ret;
			},

			isFull: function() {
				return ((this.end+1) % this.size) == this.start;
			},

			isEmpty: function() {
				return (this.end == this.start);
			}
		};

		return CircularBuffer;
    }
);