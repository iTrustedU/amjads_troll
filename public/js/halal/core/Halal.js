define(
	'halal/core/Halal', 

	[
 	  	"loglevel",
 	  	"halal/core/UUID",
 	 	"halal/core/StateManager",
 	 	"halal/shims/RAFShim",
 	 	"halal/core/Timer",
 	 	"halal/core/ScreenManager",
 	 	"halal/core/CircularBuffer",
 	 	"modernizr",
 	 	"halal/renderers/CanvasRenderer",
 	 	"halal/core/AssetManager",
 	 	"halal/core/Storage",
 	 	"halal/core/DeferredObject",
 	 	"halal/core/OnFrameQueue"
 	], 

 	/*@todo izbaciti jquery odavde */
	function(Log, 
			 UUID, 
			 StateManager, 
			 RAFShim, 
			 Timer, 
			 ScreenManager, 
			 CircularBuffer,
			 Modernizr, 
			 CanvasRenderer, 
			 AssetManager,
			 Storage,
			 DeferredObject,
			 OnFrameQueue) {


		var frame_buffer = new OnFrameQueue(),
			paused = true,
			fps_counter = 0,
			is_chrome = undefined,
			cap_fps = 30;

		/* = window.Halal = function(query, callb) {

		};*/

		StateManager.register([
			"ENGINE_COLD", 
			"ENGINE_STARTED",
			"ENGINE_LOADED",
			"ENGINE_PAUSED",
			"ENTER_FRAME",
			"FPS_UPDATED",
			"STATISTICS_UPDATED"
		]);
		StateManager.trigger("ENGINE_COLD");

		var Halal = {};

		Halal.AssetManager 	= AssetManager;
		Halal.StateManager 	= StateManager;
		Halal.Timer 		= Timer;
		Halal.ScreenManager = ScreenManager;
		Halal.Renderer 		= CanvasRenderer;
		Halal.Storage		= Storage;
		Halal.Config 		= undefined;


		Halal.Timer.each(1000, function() {
			StateManager.trigger("FPS_UPDATED", fps_counter);
			fps_counter = 0;
		});

		StateManager.when("ENGINE_PAUSED", function() {
			paused = true;
		});

		Halal.init = function(file) {
			Log.enableAll();
			var defer = new DeferredObject();
			AssetManager.loadConfig(file).done(function(cfg) {
				Halal.Config = cfg;
				Halal.start();
				defer.resolve();
			});
			return defer.promise();
		};

		function render_loop() {
			window.requestAnimFrame(render_loop)
			fps_counter++;
			ScreenManager.update(Timer.delta);
			ScreenManager.render(Timer.delta);
			Timer.tick();
		};

		Halal.start = function() {
			StateManager.trigger("ENGINE_STARTED");
			setTimeout(render_loop, 0);
		};

		Halal.pushScreen = function(screen) {
			ScreenManager.manage(screen);
		}

		return Halal;
	}
);