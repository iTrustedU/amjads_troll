define(
    "halal/core/DeferredObject",
 
    [],

    function() {

    	var Promise = function() {
    		this.chain = [];

    		this.exec = function(args) {
    			var callb = undefined;
    			while((callb = this.chain.pop())) {
    				callb(args);
    			}
    		};
    		
    		this.done = function(callb) {
    			this.chain.push(callb);
    			return this;
    		};
    	};

		var DeferredObject = function(numEvents) {
			this.num_events = numEvents || 0;
			this.prom = new Promise();

			this.resolve = function(args) {
                if(this.num_events == 0) {
					this.prom.exec(args);
				}
			};

			this.promise = function() {
				return this.prom;
			};

			this.acquire = function(args) {
				this.num_events--;
				if(this.num_events == 0) {
					/*notetoself: numEvents is in outter function*/
					this.prom.exec(args || numEvents);
				}
			};
		};

		return DeferredObject;
    }
);