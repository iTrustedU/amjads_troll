define('halal/renderers/CanvasRenderer', 
['jquery'],

function($) {
	var ctx = undefined;
	var canvas = undefined;
	var clearColor = "#ffffcc";
	var CanvasRenderer = {};
	var $renderspace = $('.renderspace');
	var in_fullscreen = false;

	var origin = "center top";
	var avail_width = undefined;
	var avail_height = undefined;
	
	var style = undefined;

	function initRenderer() {
		var width = $renderspace.innerWidth(),
			height = $renderspace.innerHeight();
			
		var $canvas = $("<canvas>", {
			"class" : "game"
		});

		$canvas.html("Your browser does not support HTML5 canvas");
		$canvas.prependTo($renderspace);

		canvas = $canvas.get(0);
		
		style = canvas.getAttribute('style') || '';
	    
	    console.log(width + " , " + height);
		canvas.width = width;
		canvas.height = height;

		ctx = canvas.getContext('2d');

		CanvasRenderer.ctx = ctx;
		CanvasRenderer.width = width;
		CanvasRenderer.height = height;
		CanvasRenderer.rect = canvas.getBoundingClientRect();
	}

	document.addEventListener("fullscreenchange", function () {
        in_fullscreen = document.fullscreenElement;
    }, false);
        
    document.addEventListener("mozfullscreenchange", function () {
        in_fullscreen = document.mozFullScreen;
    }, false);
    
    document.addEventListener("webkitfullscreenchange", function () {
        in_fullscreen = document.webkitIsFullScreen;
    }, false);

    window.addEventListener("resize", function() {
    	if(!in_fullscreen) {
    		console.log("fullscreen exited");
    		origin = "top top";
    		scale = "1, 1";
        	$renderspace.get(0).setAttribute('style', style + ' ' + '-ms-transform-origin: ' + origin + '; -webkit-transform-origin: ' + origin + '; -moz-transform-origin: ' + origin + '; -o-transform-origin: ' + origin + '; transform-origin: ' + origin + '; -ms-transform: scale(' + scale + '); -webkit-transform: scale3d(' + scale + ', 1); -moz-transform: scale(' + scale + '); -o-transform: scale(' + scale + '); transform: scale(' + scale + ');');
    	} else {
    		console.log("fullscreen entered");
        	
    	}
    }, false);


	CanvasRenderer.goFullScreen = function() {
	    in_fullscreen = !in_fullscreen;

    	if(in_fullscreen) {
    		avail_width = window.screen.availWidth;
    		avail_height = window.screen.availHeight;
    	} else {
    		avail_height = document.body.clientHeight;
    		avail_width = document.body.clientWidth;
    	}

        var scale = {x: 1, y: 1};
        var scX = (avail_width) / $renderspace.width();
        var scY = (avail_height) / $renderspace.height();

        if (scale.x < 1 || scale.y < 1) {
            scale = "1, 1";
        } else if (scale.x <= scale.y) {
            scale = scX + ', ' + scX;
        } else {
            scale = scY + ', ' + scY;
        }
		
		/* ovde pravi haos zbog jebenih browsera i njihovih ratova */
		if($renderspace.get(0).mozRequestFullScreen) {
			origin = "left top";
			$renderspace.get(0).mozRequestFullScreen();
		} else if($renderspace.get(0).webkitRequestFullScreen) {
			origin = "center center";
			$renderspace.get(0).webkitRequestFullScreen(); 
		} else if($renderspace.get(0).requestFullscreen) {
			origin = "left top";		
			$renderspace.get(0).requestFullscreen();
		}
		$renderspace.get(0).setAttribute('style', style + ' ' + '-ms-transform-origin: ' + origin + '; -webkit-transform-origin: ' + origin + '; -moz-transform-origin: ' + origin + '; -o-transform-origin: ' + origin + '; transform-origin: ' + origin + '; -ms-transform: scale(' + scale + '); -webkit-transform: scale3d(' + scale + ', 1); -moz-transform: scale(' + scale + '); -o-transform: scale(' + scale + '); transform: scale(' + scale + ');');
	};

	CanvasRenderer.drawImage = function(img, x, y, width, height) {
		ctx.drawImage(img, x, y, width, height);
	}

	CanvasRenderer.drawImageFromSource = function(img, sx, sy, sw, sh, x, y, width, height) {
		ctx.drawImage(img, sx,sy,sw,sh,x,y,width,height);
	}

	CanvasRenderer.drawSprite = function(sprite, x, y) {
		ctx.drawImage(
			sprite.img, 
			sprite.x, 
			sprite.y,
			sprite.w,
			sprite.h,
			x,
			y,
			sprite.w,
			sprite.h
		);
	}

	CanvasRenderer.strokeRect = function(color, rect) {
		ctx.strokeStyle = color;
		ctx.strokeRect(rect.x, rect.y, rect.w, rect.h);
	}

	CanvasRenderer.clear = function() {
		//ctx.fillStyle = clearColor;
		ctx.clearRect(0, 0, canvas.width, canvas.height);
	}

	CanvasRenderer.getBoundingClientRect = function() {
		return canvas.getBoundingClientRect();
	}

	CanvasRenderer.strokeCircle = function(circle, strColor) {
		ctx.strokeStyle = strColor;
		ctx.beginPath();
		ctx.arc(circle.x, circle.y, circle.r, 0, Math.PI*2, false);
		ctx.stroke();
	}

	initRenderer();

	return 	CanvasRenderer;
});