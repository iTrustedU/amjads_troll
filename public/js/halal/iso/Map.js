define(
    "halal/iso/Map",
 
    ["halal/core/StateManager",
     "halal/renderers/CanvasRenderer",
     "halal/core/DOMManager",
     "halal/geometry/Vec2",
     "halal/core/AssetManager"],

    function(StateManager, Renderer, DOMManager, Vec2, AssetManager) {
    	"use strict";

    	var mouse_down = false;

		var client_rect = DOMManager.getRect();

		var client_rect_left = client_rect.left;
		var client_rect_top = client_rect.top;

		var num_rows = 5;
		var num_cols = 5;

    	var tiles = [];

    	var tile_w = 128;
    	var tile_h = 64;

    	var center = Vec2.fromValues(client_rect.width/2, client_rect.height/2);

    	var camera = Vec2.fromValues(0, 0);
    	//center = twoDToIso(center[0], center[1]);

    	var spr = AssetManager.getSprite("tiles_0", "beze_tile_1.png");
    	var spr1 = AssetManager.getSprite("horses", "deer.png");

    	var Map = function Map(rows, cols) {
    		var center_row = rows/2;
    		var center_col = cols/2;

    		num_cols = cols;
    		num_rows = rows;
    		center[1] -= (tile_h*num_cols)/2;
    	
    		console.log(spr);
    		for(var i = 0; i < rows; ++i) {
    			for(var j = 0; j < cols; ++j) {
    				var x = i * tile_w/2;
    				var y = j * tile_h;
    				var to = twoDToIso(x, y);
					put_at({
						first: to.x  + center[0],
						second: to.y + center[1],
						third: 0,
						fourth: 0,
						sprite: spr
					}, i, j);
    			}
    		}

    		//Renderer.ctx.translate(center[0], center[1]);
    		get_at(0, 0).sprite = spr1;
    		get_at(0, 1).sprite = spr1;
    		get_at(0, 2).sprite = spr1;

    		Renderer.ctx.strokeStyle = "gray";
    	};

    	function twoDToIso(x, y) {
		  var tempPt = {
		  	x: 0,
		  	y: 0
		  }
		  tempPt.x = x - y;
		  tempPt.y = (x + y) / 2;
		  return tempPt;
		};

		function isoTo2D(pt) {
		  return { 
		  	x: (2 * pt.y + pt.x) / 2,
		  	y: (2 * pt.y - pt.x) / 2
		  }
		};

    	function put_at(obj, row, col) {
    		tiles[row * num_rows + col] = obj;
    	};

    	function get_at(row, col) {
    		return tiles[row * num_rows + col];
    	}
    	Map.prototype = {
			render: function() {
				Renderer.ctx.clearRect(0, 0, Renderer.width, Renderer.height);
				for(var i = 0; i < tiles.length; ++i) {
					var x = tiles[i].first + camera[0];
					var y = tiles[i].second + camera[1];
					Renderer.ctx.beginPath();
					Renderer.ctx.moveTo(x, y);
					Renderer.ctx.lineTo(x+tile_h, y+tile_h/2);
					Renderer.ctx.lineTo(x, y+tile_h);
					Renderer.ctx.lineTo(x-tile_h, y+tile_h/2);
					Renderer.ctx.closePath();
					Renderer.ctx.stroke();
					//Renderer.drawImage(tiles[i].sprite.img, tiles[i].first - tile_h, tiles[i].second, tile_w, tile_h);
					//Renderer.ctx.fillRect(tiles[i].first, tiles[i].second, 5, 5);
				}
				//Renderer.ctx.fillRect(center[0], center[1], 2, 2);
			},

			update: function() {
				
			}
		}

		Renderer.ctx.font="10px Arial";

		var prev_cam_x = Renderer.width/2;
		var prev_cam_y = Renderer.height/2;

		window.addEventListener("mousemove", function(evt) {
			var pt = getMousePos(evt);
			if(mouse_down) {

				camera[0] = (pt.x - mouse_down.x);
			}
			var str = pt.
			 + ", " + pt.y;
			var width = Renderer.ctx.measureText(str).width;
			
			Renderer.ctx.fillText(str, 10, 10);
		});

		window.addEventListener("mousedown", function(evt) {
			mouse_down = getMousePos(evt);
			//prev_cam_x = mouse_down.x;
			//prev_cam_y = mouse_down.y;

			console.log(mouse_down);
		});

		window.addEventListener("mouseup", function(evt) {
			mouse_down = undefined;
			var pt = getMousePos(evt);
			//console.log(Renderer.width);
			//console.log(Renderer.height);
			/*if(evt.y > Renderer.height/2) {
				//camera[1] =  -evt.y + Renderer.height/2;
			} else {
				//camera[0] =  evt.x - Renderer.width/2;
			}
			camera[1] =  -evt.y + Renderer.height/2;
			camera[0] = -evt.x + Renderer.width/2;
			//Renderer.ctx.translate(1, 0);
			//Renderer.ctx.clearRect(0, 0, Renderer.width, Renderer.height);
			//Renderer.ctx.translate(128, 0);
			/*var pt = getMousePos(evt);
			pt.x -= center[0];
			pt.y -= center[1];
			var iso = isoTo2D(pt);
			var coord = get_coord(iso);
			get_at(coord.x, coord.y).sprite = spr1;
			console.log(coord.x, coord.y);*/
		});


  		function get_coord(isopt) {
  			return {
  				x: Math.floor(isopt.x / tile_h),
  				y: Math.floor(isopt.y / tile_h)
  			}
  		}

	    function getMousePos(evt) {
	        return {
	        	x: (evt.clientX - Renderer.rect.left),
	        	y: (evt.clientY - Renderer.rect.top)
	        }
	    }

		/*var cell_size = 32; //pixels
		var map_array = [];

		var client_rect = DOMManager.getRect();

		var client_rect_left = client_rect.left;
		var client_rect_top = client_rect.top;

		var Map = function(rows, columns) {
			this.rows = rows;
			this.columns = columns;

			for(var i = 0; i < rows; ++i) {
				for(var j = 0; j < columns; ++j) {
					map_array.push({
						x: j*cell_size,
						y: i*cell_size,
						w: cell_size,
						h: cell_size,
					});
				}
			}
		};	

		var cur_cell = undefined;
		Map.prototype = {
			draw: function() {
				StateManager.when("ENTER_FRAME", function(delta) {
					
					for(var i = 0; i < map_array.length; ++i) {
						cur_cell = map_array[i];
						Renderer.strokeRect("#cfbdaf", cur_cell);
					}

				});
			},
			putAt: function(row, column) {

			}	
		};

		var cur_point = undefined;
		var to_point = undefined;
		window.addEventListener("mousedown", function(evt) {
			cur_point = getMousePos(evt);
			console.log(Renderer.ctx.currentTransform);
		});

		window.addEventListener("mouseup", function() {
			cur_point = undefined;
		})

		window.addEventListener("mousemove", function(evt) {
			if(cur_point) {
				to_point = getMousePos(evt);
				var diff_x = to_point.x - cur_point.x;
				var diff_y = to_point.y - cur_point.y;
				

				//Renderer.clear();
				//Renderer.ctx.save();
				Renderer.ctx.setTransform(1,0,0,1,diff_x, diff_y);
				//Renderer.ctx.clearRect(, canvas.width, canvas.height);
				//Renderer.clear();
			}
		});

		return Map;
		*/
		return Map;
    }
);