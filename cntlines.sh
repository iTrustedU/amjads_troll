#!/bin/bash
find ./$1 -name \*.js -print | sed -e 's/\.\///' | xargs wc -l | tail -1
